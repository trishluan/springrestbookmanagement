package com.example.demo.controller;

import com.example.demo.models.Author;
import com.example.demo.models.User;
import com.example.demo.pojo.Book;
import com.example.demo.repositories.AuthorRepository;
import com.example.demo.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class MyController {

    @Autowired
    UserRepository userRepository;
    @Autowired
    AuthorRepository authorRepository;

    @GetMapping("/sum")
    Integer sum(@RequestParam("a") Integer one,
                @RequestParam("b") Integer two) {
        return one + two;
    }

    @GetMapping("/giaithua")
    Integer giaiThua(@RequestParam("a") Integer one) {
        int giaiThua = 1;
        if (one <= 1) {
            giaiThua = 1;
        } else {
            for (int i = 2; i <= one; i++) {
                giaiThua *= i;
            }
        }
        return giaiThua;
    }

    @PostMapping("/save-book")
    String saveBook(@RequestBody Book book) {
        return book.getName();
    }

    @PostMapping("/create-user")
    void createUser(@RequestBody User user) {
        userRepository.save(user);
    }

    @PostMapping("/create-author")
    void createAuthor(@RequestBody Author author) {
        authorRepository.save(author);
    }
}
